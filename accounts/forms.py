from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User


class AddAdminForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'username')


class AddUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'username')
