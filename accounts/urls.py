from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, reverse_lazy

from accounts import views

urlpatterns = [
    path('', views.DashboardView.as_view(), name="dashboard"),
    path('login/', LoginView.as_view(template_name='backend/accounts/login.html'), name='login'),
    path('logout/', LogoutView.as_view(next_page=reverse_lazy('login')), name='logout'),
    path('admin/add/', views.AdminCreateView.as_view(), name="admin-new"),
    path('admin/list/', views.AdminListView.as_view(), name="admin-list"),
    path('user/add/', views.UserCreateView.as_view(), name="user-new"),
    path('user/list/', views.UserListView.as_view(), name="user-list"),
]