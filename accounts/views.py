from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required

from notices.models import Notice
from projects.models import Project
from setting.models import Setting

User = get_user_model()
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, CreateView, ListView
from accounts.models import Profile
from accounts.forms import AddAdminForm, AddUserForm


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class DashboardView(TemplateView):
    template_name = 'backend/dashboard.html'

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['newest_users'] = User.objects.all()[:5]
        context['newest_projects'] = Project.objects.all()[:10]
        context['newest_notices'] = Notice.objects.all()[:5]
        context['total_users'] = User.objects.all().count()
        context['total_projects'] = Project.objects.all().count()
        context['total_notices'] = Notice.objects.all().count()
        return context


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class AdminCreateView(SuccessMessageMixin, CreateView):
    template_name = 'backend/accounts/add_admin.html'
    form_class = AddAdminForm

    @transaction.atomic()
    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        Profile.objects.create(user=user)
        messages.success(self.request, 'An admin added successfully.')
        return HttpResponseRedirect(reverse('admin-new'))


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class AdminListView(ListView):
    template_name = 'backend/accounts/admin_list.html'

    model = User

    # paginate_by = 1  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = User.objects.all().filter(is_staff=True).order_by('-date_joined')
        return context


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class UserCreateView(SuccessMessageMixin, CreateView):
    template_name = 'backend/accounts/users/user_add.html'
    form_class = AddUserForm

    @transaction.atomic()
    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_superuser = False
        user.is_staff = False
        user.save()
        Profile.objects.create(user=user)
        messages.success(self.request, 'An user added successfully.')
        return HttpResponseRedirect(reverse('user-new'))


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class UserListView(ListView):
    template_name = 'backend/accounts/users/user_list.html'
    model = User

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.all().filter(is_staff=False).order_by('-date_joined')
        return context


class HomepageView(TemplateView):
    template_name = 'frontend/home.html'

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['newest_projects'] = Project.objects.all()[:6]
        context['setting'] = Setting.objects.all()[0]

        return context
