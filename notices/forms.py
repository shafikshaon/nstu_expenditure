from django import forms
from django.forms import ModelForm

from notices.models import Notice


class AddNoticeForm(ModelForm):
    class Meta:
        model = Notice
        fields = ('__all__')
