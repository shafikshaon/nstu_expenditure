import os
import uuid
from datetime import datetime

from django.db import models
from django.utils.deconstruct import deconstructible

from accounts.models import User


@deconstructible
class RandomFileName(object):
    def __init__(self, path):
        self.path = os.path.join(path, "%s%s")

    def __call__(self, _, filename):
        extension = os.path.splitext(filename)[1]
        return self.path % (uuid.uuid4(), extension)


class Notice(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    title = models.CharField(max_length=200, null=False, blank=False)
    description = models.TextField(null=True, blank=True)
    published_date = models.DateField(null=True, blank=True)
    attachment = models.FileField(upload_to=RandomFileName('static/upload/notice/'), null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
