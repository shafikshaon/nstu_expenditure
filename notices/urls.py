from django.urls import path

from notices import views

urlpatterns = [
    path('notice/add/', views.NoticeCreateView.as_view(), name="notice-add"),
    path('notice/list/', views.NoticeListView.as_view(), name="notice-list"),
]
