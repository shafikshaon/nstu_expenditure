from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, CreateView, ListView

from accounts.models import User
from notices.forms import AddNoticeForm
from notices.models import Notice
from setting.models import Setting


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class NoticeCreateView(SuccessMessageMixin, CreateView):
    template_name = 'backend/notices/notice_add.html'
    form_class = AddNoticeForm

    @transaction.atomic()
    def form_valid(self, form):
        notice = form.save(commit=False)
        notice.user = self.request.user
        notice.save()
        messages.success(self.request, 'A notice added successfully.')
        return HttpResponseRedirect(reverse('notice-add'))

    def form_invalid(self, form):
        print (form.errors.as_data())
        pass


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class NoticeListView(ListView):
    template_name = 'backend/notices/notice_list.html'
    model = Notice
    ordering = ['-created_at']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['users'] = User.objects.all()
        return context


class FrontNoticeListView(ListView):
    template_name = 'frontend/notices/list.html'
    model = Notice
    ordering = ['-created_at']
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['users'] = User.objects.all()
        context['setting'] = Setting.objects.all()[0]
        return context


