"""nstu_expenditure URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import debug_toolbar
from django.views.generic import TemplateView

from accounts import views as account_views
from projects import views as project_views
from setting import views as setting_views
from notices import views as notice_views

urlpatterns = [
    path('admin/', include('accounts.urls')),
    path('admin/', include('projects.urls')),
    path('admin/', include('notices.urls')),
    path('admin/', include('setting.urls')),

    path('', account_views.HomepageView.as_view(), name="homepage"),
    path('project/<slug:slug>', project_views.ProjectDetailView.as_view(), name="project-detail"),
    path('project/category/all/', project_views.FrontProjectListView.as_view(), name="project-list-front"),
    path('project/category/upcoming/', project_views.FrontUpcomingProjectListView.as_view(), name="project-list-upcoming"),
    path('project/category/in-progress/', project_views.FrontInProgressProjectListView.as_view(), name="project-list-in-progress"),
    path('project/category/completed/', project_views.FrontCompletedProjectListView.as_view(), name="project-list-completed"),

    path('contact/', setting_views.ContactCreateView.as_view(), name="contact-create"),


    path('notice/', notice_views.FrontNoticeListView.as_view(), name="notice-list-front"),

    path('__debug__/', include(debug_toolbar.urls)),
]
