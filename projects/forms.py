from django import forms
from django.forms import ModelForm

from projects.models import Project


class AddProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ('__all__')
