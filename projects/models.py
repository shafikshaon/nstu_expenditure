import os
import random
import string
import uuid

from django.db import models
from django.utils.deconstruct import deconstructible
from django.utils.text import slugify

from accounts.models import User


@deconstructible
class RandomFileName(object):
    def __init__(self, path):
        self.path = os.path.join(path, "%s%s")

    def __call__(self, _, filename):
        extension = os.path.splitext(filename)[1]
        return self.path % (uuid.uuid4(), extension)


class Project(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=False, blank=False)
    project_name = models.CharField(max_length=200, null=False, blank=False)
    description = models.TextField(null=False, blank=False)
    worker = models.IntegerField(null=False, blank=False)
    engineer = models.ForeignKey(User, on_delete=models.PROTECT, null=False, blank=False, related_name='engineer')
    conductor = models.ForeignKey(User, on_delete=models.PROTECT, null=False, blank=False, related_name='conductor')
    budget = models.DecimalField(max_digits=19, decimal_places=10, null=False, blank=False)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    administrator = models.CharField(null=True, blank=True, max_length=100)
    latitude = models.DecimalField(max_digits=19, decimal_places=10, null=True, blank=True)
    longitude = models.DecimalField(max_digits=19, decimal_places=10, null=True, blank=True)
    status = models.BooleanField(default=True)
    category = models.CharField(max_length=100, null=False, blank=False)
    slug = models.SlugField(max_length=255, null=True, blank=True, unique=True)
    master_picture = models.ImageField(upload_to=RandomFileName('static/upload/project/'),
                                       default='static/upload/project/avatar.jpg')
    picture1 = models.ImageField(upload_to=RandomFileName('static/upload/project/'), null=True, blank=True)
    picture2 = models.ImageField(upload_to=RandomFileName('static/upload/project/'), null=True, blank=True)
    picture3 = models.ImageField(upload_to=RandomFileName('static/upload/project/'), null=True, blank=True)
    picture4 = models.ImageField(upload_to=RandomFileName('static/upload/project/'), null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(
            self.project_name + ''.join(random.choice(string.ascii_uppercase + '-' + string.digits) for _ in range(5)))
        super(Project, self).save(*args, **kwargs)

# def project_pre_save(signal, instance, sender, **kwargs):
#     if not instance.slug:
#         slug = slugify(instance.slug)
#         new_slug = slug
#         count = 0
#         while Project.objects.filter(slug=new_slug).exclude(id=instance.id).count() > 0:
#             count += 1
#             new_slug = '{slug}-{count}'.format(slug=slug, count=count)
#
#         instance.slug = new_slug
#
#
# signals.pre_save.connect(project_pre_save, sender=Project)
