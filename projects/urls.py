from django.urls import path

from projects import views

urlpatterns = [
    path('project/add/', views.ProjectCreateView.as_view(), name="project-add"),
    path('project/edit/<slug>', views.ProjectUpdateView.as_view(), name="project-edit"),
    path('project/list/', views.ProjectListView.as_view(), name="project-list"),

]
