from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DetailView

from accounts.models import User
from notices.models import Notice
from projects.forms import AddProjectForm
from projects.models import Project
from setting.models import Setting


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class ProjectCreateView(SuccessMessageMixin, CreateView):
    template_name = 'backend/projects/add_project.html'
    form_class = AddProjectForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.filter(is_staff=False)
        return context

    @transaction.atomic()
    def form_valid(self, form):
        project = form.save(commit=False)
        project.user = self.request.user
        project.save()
        messages.success(self.request, 'A project added successfully.')
        return HttpResponseRedirect(reverse('project-add'))


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class ProjectUpdateView(UpdateView):
    template_name = 'backend/projects/project_edit.html'
    model = Project
    # form_class = AddProjectForm
    fields = ['project_name', 'worker', 'conductor', 'engineer', 'description', 'budget', 'start_date', 'end_date',
              'administrator', 'latitude', 'longitude', 'status', 'category', 'master_picture', 'picture1', 'picture2',
              'picture3', 'picture4']
    success_url = '/admin/project/list'
    success_message = 'A project updated successfully.'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.filter(is_staff=False)
        return context

    # @transaction.atomic()
    # def form_valid(self, form):
    #     project = form.save(commit=False)
    #     project.user = self.request.user
    #     project.save()
    #     messages.success(self.request, 'A project updates successfully.')
    #     return HttpResponseRedirect(reverse('project-list'))


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class ProjectListView(ListView):
    template_name = 'backend/projects/project_list.html'
    model = Project
    ordering = ['-created_at']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.all()
        return context


class ProjectDetailView(DetailView):
    template_name = 'frontend/projects/details.html'
    model = Project

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['setting'] = Setting.objects.all()[0]
        context['newest_project'] = Project.objects.all()[:5]
        return context


class FrontProjectListView(ListView):
    template_name = 'frontend/projects/list-all.html'
    model = Project
    ordering = ['-created_at']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['setting'] = Setting.objects.all()[0]
        context['users'] = User.objects.all()
        return context

class FrontUpcomingProjectListView(ListView):
    template_name = 'frontend/projects/list-upcoming.html'
    model = Project
    ordering = ['-created_at']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['setting'] = Setting.objects.all()[0]
        context['users'] = User.objects.all()
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(category__exact=1)


class FrontInProgressProjectListView(ListView):
    template_name = 'frontend/projects/list-in-progress.html'
    model = Project
    ordering = ['-created_at']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['setting'] = Setting.objects.all()[0]
        context['users'] = User.objects.all()
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(category__exact=2)

class FrontCompletedProjectListView(ListView):
    template_name = 'frontend/projects/list-completed.html'
    model = Project
    ordering = ['-created_at']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['setting'] = Setting.objects.all()[0]
        context['users'] = User.objects.all()
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(category__exact=3)