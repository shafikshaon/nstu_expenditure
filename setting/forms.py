from django import forms
from django.forms import ModelForm

from setting.models import Setting, Contact


class AddSettingForm(ModelForm):
    class Meta:
        model = Setting
        fields = ('__all__')


class AddContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ('__all__')