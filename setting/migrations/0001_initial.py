# Generated by Django 2.1.2 on 2018-10-09 09:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import setting.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(blank=True, max_length=200, null=True)),
                ('about_us', models.TextField(blank=True, null=True)),
                ('latitude', models.DecimalField(blank=True, decimal_places=10, max_digits=19, null=True)),
                ('longitude', models.DecimalField(blank=True, decimal_places=10, max_digits=19, null=True)),
                ('phone_number', models.CharField(blank=True, max_length=15, null=True)),
                ('logo', models.ImageField(blank=True, null=True, upload_to=setting.models.RandomFileName('static/upload/setting/'))),
                ('favicon', models.ImageField(blank=True, null=True, upload_to=setting.models.RandomFileName('static/upload/setting/'))),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted_at', models.DateField(blank=True, null=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
