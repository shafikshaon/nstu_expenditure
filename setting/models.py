import os
import uuid
from datetime import datetime

from django.db import models
from django.utils.deconstruct import deconstructible

from accounts.models import User


@deconstructible
class RandomFileName(object):
    def __init__(self, path):
        self.path = os.path.join(path, "%s%s")

    def __call__(self, _, filename):
        extension = os.path.splitext(filename)[1]
        return self.path % (uuid.uuid4(), extension)


class Setting(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    about_us = models.TextField(null=True, blank=True)
    latitude = models.DecimalField(max_digits=19, decimal_places=10, null=True, blank=True)
    longitude = models.DecimalField(max_digits=19, decimal_places=10, null=True, blank=True)
    phone_number = models.CharField(max_length=15, null=True, blank=True)
    logo = models.ImageField(upload_to=RandomFileName('static/upload/setting/'), null=True, blank=True)
    favicon = models.ImageField(upload_to=RandomFileName('static/upload/setting/'), null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)


class Contact(models.Model):
    full_name = models.CharField(max_length=200, null=False, blank=False)
    email = models.EmailField(max_length=100, null=False, blank=False)
    message = models.TextField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
