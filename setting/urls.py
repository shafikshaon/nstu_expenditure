from django.urls import path

from setting import views

urlpatterns = [
    path('setting/basic_info/add/', views.SettingCreateView.as_view(), name="basic-information-add"),
    path('setting/basic_info/edit/<pk>', views.SettingUpdateView.as_view(), name="setting-edit"),
    path('setting/basic_info/list/', views.SettingListView.as_view(), name="basic-information-list"),
]
