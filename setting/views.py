from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, CreateView, ListView, UpdateView

from accounts.models import User
from setting.forms import AddSettingForm, AddContactForm
from setting.models import Setting, Contact


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class SettingCreateView(SuccessMessageMixin, CreateView):
    template_name = 'backend/setting/basic_information_add.html'
    form_class = AddSettingForm

    def dispatch(self, request, *args, **kwargs):
        if Setting.objects.all().count() > 0:
            return redirect('basic-information-list')

        return super().dispatch(request, *args, **kwargs)

    @transaction.atomic()
    def form_valid(self, form):
        setting = form.save(commit=False)
        setting.save()
        messages.success(self.request, 'Basic information added successfully.')
        return HttpResponseRedirect(reverse('basic-information-add'))


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class SettingUpdateView(UpdateView):
    template_name = 'backend/setting/basic_information_edit.html'
    model = Setting
    fields = ['address', 'email', 'about_us', 'latitude', 'longitude', 'phone_number', 'logo', 'favicon']
    success_url = '/admin/setting/basic_info/list/'
    success_message = 'Setting updated successfully.'


@method_decorator([login_required(login_url='/admin/login/')], name='dispatch')
class SettingListView(ListView):
    template_name = 'backend/setting/basic_information_list.html'
    model = Setting
    ordering = ['-created_at']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class ContactCreateView(SuccessMessageMixin, CreateView):
    template_name = 'frontend/setting/contact.html'
    form_class = AddContactForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['setting'] = Setting.objects.all()[0]
        context['users'] = User.objects.all()
        return context

    def form_valid(self, form):
        contact = form.save(commit=False)
        contact.save()
        messages.success(self.request, 'Your message sent successfully.')
        return HttpResponseRedirect(reverse('contact-create'))
    #
    # def form_invalid(self, form):
    #     print (form.errors.as_data())
    #     pass
